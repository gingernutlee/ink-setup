module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    watch: {
      html: {
        files: ['creatives/**/*.html']
      }
    },
    browserSync: {
      bsFiles: {
        src : 'creatives/**/*'
      },
      options: {
        server: {
          baseDir: "./"
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-browser-sync');

  grunt.registerTask('default', ['browserSync', 'watch']);
};